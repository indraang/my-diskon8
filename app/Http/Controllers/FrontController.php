<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ShopProduct;
use App\Category;
use App\RajaOngkir;
use App;
use DB;
class FrontController extends Controller
{
    private $dataShop;
    private $baseUrl;

    public function __construct()
    {
        $this->baseUrl = App::make('url')->to('/');
        $this->dataShop = DB::table('shops')->where('domain', $this->baseUrl)->first();
        $this->rajaOngkir = new RajaOngkir();
    }

    public function index()
    {
        //$data['collectionfront']= Product::get();
        /*if(!$this->dataShop){
            return redirect('https://ultimate246.com/admin');
        }*/
        $data['collectionfront']= ShopProduct::select('shop_products.*','products.id as product_id', 'products.name', 'products.name', 'products.slug', 'products.brand', 'products.discount', 'products.main_image', 'products.status')
        ->leftJoin('products', 'shop_products.product_id', '=', 'products.id')
        ->where('shop_products.shop_id', $this->dataShop->id)
        ->orderBy('products.created_at', 'desc')->get();
        $data['category'] = Category::get();
        
        $data['frontslide'] = DB::table('frontslides')->where('shop_id',$this->dataShop->id)->get();
        return view('fronts.front',$data);
    }
    public function productdetail($id=null)
    {

        $data['collection']= ShopProduct::select('shop_products.*', 'products.name', 'products.name', 'products.slug', 'products.brand', 'products.discount', 'products.main_image', 'products.status')
        ->leftJoin('products', 'shop_products.product_id', '=', 'products.id')
        ->where('products.id', $id)
        ->where('shop_products.shop_id', $this->dataShop->id)
        ->first();

        $data['slidePicture']     = DB::table('product_assets')->where('product_id',$id)->where('type','model')->get();
        $data['benefit']          = DB::table('benefitContent')->limit(4)->get();
        $data['productPicture']   = DB::table('product_assets')->where('product_id',$id)->get();
        $data['productComment']   = DB::table('globalComment')->get();
        $data['productPictureFooter']  = DB::table('productPicture')->where('type','footer')->where('idProduct',@$id)->get();

        $data['globalFooter']    = DB::table('globalFooter')->get();
        $data['varian']    = DB::table('product_stocks')->where('product_id',$id)->get();
        try{
            $provinces = $this->rajaOngkir->getProvince();                    
        if ($provinces['rajaongkir']['status']['code'] === 200) {
            $data['provinceData'] = $provinces['rajaongkir']['results'];
        } else {
            $data['provinceData'] = [];
        }        
    }catch(\Exception $error){
        return $error->getMessage();
    }   
        return view('fronts.frontdetail',$data);
    }
    public function productcategory($id=null)
    {


        $data['globalFooter']    = DB::table('globalFooter')->get();
        $data['category']    = Category::get();

        $data['collection']= ShopProduct::select('shop_products.*', 'products.name', 'products.name', 'products.slug', 'products.brand', 'products.discount', 'products.main_image', 'products.status')
        ->leftJoin('products', 'shop_products.product_id', '=', 'products.id')
        ->leftJoin('categories', 'categories.id', '=', 'products.category_id')
        ->where('categories.id', $id)
        ->where('shop_products.shop_id', $this->dataShop->id)
        ->get();

        return view('fronts.category',$data);
    }

    public function getCity(Request $request)
    {
        $params['province'] = $request->provinceID;
        $cities = $this->rajaOngkir->getCity($params);
        if ($cities['rajaongkir']['status']['code'] === 200) {
            $citiesData = $cities['rajaongkir']['results'];
        } else {
            $citiesData = [];
        }

        return $citiesData;
    }
    public function getCost(Request $request){
        $params["courier"] = 'jne';
        $params['origin'] = '673';
        $params['originType'] = 'subdistrict';
        $params['destination'] = '2096';
        $params['destinationType'] = 'subdistrict';
        $params['weight'] = 1000;

        $cost = $this->rajaOngkir->cost($params);

        if($cost["rajaongkir"]["status"]["code"] === 200){
            $totalCost = $cost['rajaongkir']['results'][0]['costs'];
        }else{
            $totalCost = [];
        }

        return $totalCost;
    }

    public function getSubdistrict(Request $request)
    {
        $params['city'] = $request->cityID;
        $subdistrict = $this->rajaOngkir->getSubdistrict($params);
        if ($subdistrict['rajaongkir']['status']['code'] === 200) {
            $subdistrictData = $subdistrict['rajaongkir']['results'];
        } else {
            $subdistrictData = [];
        }

        return $subdistrictData;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
