<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Ultimate246 - Admin</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Play:400,700" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/owl.carousel.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/owl.transitions.css') }}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/animate.css') }}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
    <!-- meanmenu icon CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/meanmenu.min.css') }}">
    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <!-- morrisjs CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/morrisjs/morris.css') }}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/scrollbar/jquery.mCustomScrollbar.min.css') }}">
    <!-- metisMenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/metisMenu/metisMenu.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/metisMenu/metisMenu-vertical.css') }}">
    <!-- calendar CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/calendar/fullcalendar.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/calendar/fullcalendar.print.min.css') }}">
    <!-- buttons CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/buttons.css')}}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{ asset('css/responsive.css') }}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{ asset('js/vendor/modernizr-2.8.3.min.js') }}"></script>
    <link href="{{ asset('vendor/datatables/dataTables.bootstrap4.min.css') }}">
    <!-- summernote CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('css/summernote/summernote.css')}}">
    <link rel="stylesheet" href="{{ asset('css/datapicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('css/modals.css') }}">

    <style media="screen">
      .table>thead>tr>th,
      .table>tbody>tr>td
      {
        text-align: center;
      }

      .product-status-wrap .element-header,
      .product-tab-list .main-sparkline10-hd {
          border-bottom: 1px solid rgba(0,0,0,0.05);
          /* padding-bottom: 1rem; */
          margin-bottom: 2rem;
          position: relative;
          z-index: 1;
      }
      .product-tab-list .element-header {
          margin-left: 20px;
          padding-top: 10px;
      }
      .product-status-wrap .element-header:after,
      .product-tab-list .main-sparkline10-hd:after {
          content: "";
          background-color: #db365f;
          width: 25px;
          height: 4px;
          border-radius: 0px;
          display: block;
          position: absolute;
          bottom: -3px;
          left: 0px;
      }

      #myTab3.tab-review-design {
          padding: 15px 15px 25px 0px;
          background: none;
      }
      #myTab3.tab-review-design li.active a:before, #myTab4.tab-review-design li.active a:before {
          height: 4px;
          background: #db365f;
      }

      .nav-pills>li>a.active {
        color: #495057;
        background-color: rgba(0,0,0,0);
        border-color: #dee2e6 #dee2e6 rgba(0,0,0,0);
        position: relative;
      }
      .nav-pills>li>a {
        color: rgba(0,0,0,0.3);
      }
      .nav-pills>li.active>a,
      .nav-pills>li.active>a:focus,
      .nav-pills>li.active>a:hover,
      .nav-pills>li>a:hover{
        background-color: #db365f;
      }

      .font-weight-bold {
          font-weight: bold !important;
      }

      button.pd-setting-ed{
        border-radius: 15px;
      }
      button.bg-pink{
        background: #db365f;
      }
      button.bg-cream{
        background: #fbe4a0;
      }
      button.bg-green{
        background: #24b314;
      }
      button.bg-yellow{
        background: #FFFF00;
      }
      button.bg-orange{
        background: #e65252;
      }
    </style>

    <style media="screen">
      @media (min-width: 768px) {
      .asset-top{
        padding-top: 40px;
        }
      }
    </style>
</head>
