@extends('layouts.member_layout')
@section('content')
<div class="content-i">
    <div class="content-box">
        <div class="element-wrapper">
            <div class="element-actions">
                <a class="btn btn-info" href="https://incerz.satujuan.id/admin/product/1727">Kembali</a>
            </div>
            <div class="element-header">
                <h3>{{@$collectionproduct->name}}</h3>
            </div>
            @include('members.products.header')
            <div class="element-box">
                <div class="os-tabs-w mb-3 p-0">
                    <div class="os-tabs-controls mb-0">
                        <ul class="nav nav-tabs upper">

                            <li class="nav-item">
                                <a aria-expanded="false" class="nav-link active"
                                    href="https://incerz.satujuan.id/admin/product/1727/asset"> Model </a>
                            </li>
                            <li class="nav-item">
                                <a aria-expanded="false" class="nav-link "
                                    href="https://incerz.satujuan.id/admin/product/1727/asset/creative-desk"> Creative
                                    Desk </a>
                            </li>
                            <li class="nav-item">
                                <a aria-expanded="false" class="nav-link "
                                    href="https://incerz.satujuan.id/admin/product/1727/asset/videos"> Videos </a>
                            </li>
                            <li class="nav-item">
                                <a aria-expanded="false" class="nav-link "
                                    href="https://incerz.satujuan.id/admin/product/1727/asset/creative-content">
                                    Creative Content </a>
                            </li>
                        </ul>

                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89687aed5_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89687aed5_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89687aed5_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896913a56_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896913a56_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896913a56_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89694f6fe_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89694f6fe_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f89694f6fe_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969dec01_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969dec01_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969dec01_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a7429c_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a7429c_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a7429c_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969732d7_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969732d7_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969732d7_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896971d56_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896971d56_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896971d56_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a91a0a_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a91a0a_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a91a0a_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969b9254_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969b9254_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8969b9254_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b7dc15_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b7dc15_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b7dc15_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a2b5e5_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a2b5e5_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a2b5e5_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b25d62_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b25d62_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b25d62_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b8031f_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b8031f_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b8031f_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d3fa85_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d3fa85_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d3fa85_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a3e257_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a3e257_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a3e257_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d2c972_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d2c972_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d2c972_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a72218_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a72218_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896a72218_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bce486_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bce486_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bce486_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b9e080_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b9e080_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896b9e080_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bcb1bd_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bcb1bd_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bcb1bd_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d66152_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d66152_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d66152_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bd4e9f_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bd4e9f_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896bd4e9f_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c62509_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c62509_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c62509_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d6b4f3_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d6b4f3_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d6b4f3_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d04ba9_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d04ba9_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896d04ba9_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896ccc79a_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896ccc79a_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896ccc79a_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="card border p-3 mb-3">
                            <img class="img-fluid"
                                src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c6ad18_540.jpg"
                                alt="Card image cap">
                            <div class="card-block text-center mt-3">
                                <a class="btn btn-primary btn-sm"
                                    download="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c6ad18_540.jpg"
                                    href="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f896c6ad18_540.jpg"
                                    target="_blank">
                                    <i class="fa fa-download"></i>
                                    Download
                                </a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
