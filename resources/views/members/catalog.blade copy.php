@extends('layouts.member_layout')
@section('content')
<div class="content-i">
<div class="content-box">
    <div class="element-wrapper">
        <div class="element-header">
            <h3>Katalog Produk</h3>
        </div>
        <div class="row">
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8a4c09fa1_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3284058906" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Office Pack</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Ransel
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 640
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 850 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Gedebage
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> BAGGU
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 255.000
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f8b7548cdf_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1323067218" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Cross Sling bag</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Ransel
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 303
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 600 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Jay Modric
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 285.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9375e9ccc_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3803248324" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Windbreaker</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 205
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 900 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Jay Modric
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 323.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/09/5bac77103fa52_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="431545810" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Zahra Scarf</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Scarf Printed
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 479
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 500 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Kamega
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 149.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c1523e381a7b_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1745989983" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Baby Seoul</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> sweater
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 552
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 750 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> ChalaWear
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 189.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/09/5b9e6c7f0eea3_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3337432678" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Cheff Sarkoci</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Sarkoci
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 339
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 400 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Kiddoz
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 99.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f98ea99f36_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3857384463" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Kaos Kaki Wudhu</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Kaos Kaki
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 560
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 650 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Naila
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 100.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9a47d5bda_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1243817684" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Verkoop Blouse</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Blouse
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 560
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 340 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Cilengkrang
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Hi!
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 249.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9ba9f1aa8_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1971217413" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Ribbon Blouse</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Blouse
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 630
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 300 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> ChalaWear
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 149.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9c64072df_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3014930202" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Outer Kimono</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Outerwear
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 453
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 250 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Cimahi Selatan
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> BySafarilla
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 175.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/09/5bacefee251b1_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="2732518402" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Rayyana Gamis</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Gamis
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 258
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 500 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Antapani (Cicadas)
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> ELSANA
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 275.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9d6b912c8_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3813687349" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Black Pride</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Sepatu
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 388
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 800 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Tujuh Belas 17
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 289.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9dfa10e82_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="4292113107" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Yomura Taktikal</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Celana Panjang Tactical
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 560
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 530 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Lengkong
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Yomura
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 289.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9f79e1d22_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3107342318" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Chino Pants Secret Pocket</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Celana Chino Panjang
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 554
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 395 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Antapani (Cicadas)
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Ane Denim
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 249.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="{{url('/member/detailproduct')}}" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0f9ff31ba1a_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1173855739" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Yomura Tunik</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Tunik
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 558
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 500 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Lengkong
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Yomura
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 190.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/112" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fada6ce414_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3820430954" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Rivera</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Gamis
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 519
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 550 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Ainal Rami
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 289.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/115" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0faeb684aa3_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="7793772" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Fairy Tale</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Gamis
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 596
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 400 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Elnore
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 249.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/116" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c10e561b0b76_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="2682251016" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Khanza</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Gamis
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 594
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 500 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Azarine
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 249.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/117" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fb0835027d_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3311726336" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Bomber Jacket</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 553
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 848 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Equator
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 349.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/118" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fb2382ddfe_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="3444648209" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Levia</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Tunik
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 998
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 400 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Almasty
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 179.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/120" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fb4b7179c0_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="2103611994" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Kinata</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Tunik
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 598
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 300 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Famela Ainun
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 179.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/122" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fb78cc0ec2_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="2408129928" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Zahra</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Gamis
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 586
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 500 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Famela Ainun
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 290.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/124" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2018/12/5c0fba9800917_360.jpg" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1765384542" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Vier Jacket</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i>
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 367
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 900 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Tarogong Kidul
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> Four&#039;T Project
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 285.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="text-success d-inline-block" style="font-size: 11px">Telah Ditambahkan</div>
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/132" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6 col-md-3">
                <div class="card mb-5">
                    <img class="card-img-top" src="https://asset-satujuan-live.sgp1.cdn.digitaloceanspaces.com/file/2019/02/5c6e85a4d42c4_360.PNG" alt="Card image cap" style="max-width: 100%; margin: 0 auto" data-pagespeed-url-hash="1789683707" onload="pagespeed.CriticalImages.checkImageForCriticality(this);">
                    <div class="card-body element-box" style="border-radius: 0; box-shadow: none; margin: 0;">
                        <div style=" white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
                            <h6 class="card-title text-primary">Tactical Jacket</h6>
                        </div>
                        <div class="description" style="font-size: 12px">
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-tag"></i> Ready Stock
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-th-list"></i> Jaket
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-circle-o"></i> 0
                                </div>
                                <div class="col-6">

                                    <i class="fa fa-inbox"></i> 93
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-balance-scale"></i> 730 gr
                                </div>
                                <div class="col-6">
                                    <i class="fa fa-map-marker"></i> Bandung
                                </div>
                            </div>
                            <div class="row mb-1">
                                <div class="col-6">
                                    <i class="fa fa-list-alt"></i> JTX
                                </div>
                                <div class="col-6">
                                    <i class="fa">Rp</i> 408.000
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer clearfix" style="border-top: 1px solid #efefef; background: #fff; padding: 1rem; border-bottom-right-radius: 4px; border-bottom-left-radius: 4px; ">
                        <div class="pull-right">
                            <a href="https://ultimate246.com/admin/catalog/product/139" class="btn btn-primary ">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
</div>
@endsection