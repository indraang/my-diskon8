@extends('layouts.member_layout')
@section('content')
<div class="content-i">
        <div class="content-box">
<div class="element-wrapper">
<div class="row">
<div class="col-6 m-auto">
    <div class="element-box">
        <div class="element-header">
            <h3>Sedang Dalam Progress Update</h3>	
        </div>
        <p>Kami sedang dalam progress update untuk system ini <a href="mailto:hello@ultimate246.com">hello@ultimate246.com</a></p>
        <p class="text-center">Atau</p>
        <div class="text-center">
            <a class="btn btn-primary" href="mailto:hello@satujuan.com"><i class="fa fa-envelope"></i> Tanya</a>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
@endsection

